
IMAGE_NAME = ansible-playbook

build_image: .env
	docker build . -t $(IMAGE_NAME)
	docker run -it --rm -v "$(shell pwd):/work:ro" --env-file .env $(IMAGE_NAME) --version

.env: 
	echo # Empty file > .env

push_patch:
	bump2version patch
	git push
	git push --tags

push_major:
	bump2version major
	git push
	git push --tags

push_minor:
	bump2version minor
	git push
	git push --tags
