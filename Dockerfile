FROM python:3.8.6-alpine3.11

RUN apk add --no-cache \
      openssl \
      openssh-client \
      ca-certificates

RUN apk add --no-cache --virtual \
      build-dependencies \
      python-dev \
      libffi-dev \
      openssl-dev \
      build-base
RUN apk add --no-cache openssl ca-certificates


COPY requirements.txt requirements.txt
RUN pip3 install --no-cache  --requirement requirements.txt

RUN mkdir -p /work
WORKDIR /work

RUN addgroup ansible && adduser -D -G ansible ansible
USER ansible

ENTRYPOINT ["ansible-playbook"]
CMD ["--help"]
